
    // Slider
    var overlay = document.querySelector('._customer-slider__viewport'),
        slides = document.querySelectorAll('._customer-slider__slide'),
        previews = document.querySelectorAll('._customer-slider__preview-img'),
        currentSlide = 0,
        currentPreview = 0,
        previewArr = [];

    for (var i = 0; i < previews.length; i++) {
        previewArr.push(previews[i]);
        previews[i].addEventListener('click', function(e) {
            slides[currentSlide].className = '_customer-slider__slide';
            previews[currentPreview].className = '_customer-slider__preview-img';
            var index = previewArr.indexOf(e.target);
            currentSlide = index;
            currentPreview = index;
            slides[currentSlide].className = '_customer-slider__slide _customer-slider__slide_active';
            previews[currentPreview].className = '_customer-slider__preview-img _customer-slider__preview-img_active';
        });
    }

    function zoomSlide() {
        overlay.classList.toggle("_customer-slider__viewport_fullscreen");
    }
    
    window.addEventListener('resize', function(){
        overlay.classList.remove("_customer-slider__viewport_fullscreen");
    });

    function nextSlide() {
        slides[currentSlide].className = '_customer-slider__slide';
        previews[currentPreview].className = '_customer-slider__preview-img';
        currentSlide = (currentSlide + 1) % slides.length;
        currentPreview = (currentPreview + 1) % previews.length;
        slides[currentSlide].className = '_customer-slider__slide _customer-slider__slide_active';
        previews[currentPreview].className = '_customer-slider__preview-img _customer-slider__preview-img_active';
    }

    function prevSlide() {
        slides[currentSlide].className = '_customer-slider__slide';
        previews[currentPreview].className = '_customer-slider__preview-img';
        if (currentSlide == 0) {
            currentSlide = slides.length - 1;
            currentPreview = previews.length - 1;
            slides[currentSlide].className = '_customer-slider__slide _customer-slider__slide_active';
            previews[currentPreview].className = '_customer-slider__preview-img _customer-slider__preview-img_active';
        } else {
            currentSlide = (currentSlide - 1) % slides.length;
            currentPreview = (currentPreview - 1) % previews.length;
            slides[currentSlide].className = '_customer-slider__slide _customer-slider__slide_active';
            previews[currentPreview].className = '_customer-slider__preview-img _customer-slider__preview-img_active';
        }
    }
    // end Slider
